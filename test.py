from flask_restful import reqparse, abort, Resource
import requests

def main():
    port = 9797
    endpoint = "illness"
    url = "http://localhost:" + str(port) + "/"+ endpoint
    req = requests.put(url, data = {"name" : "deneme9", "category" : "deneme9", "history" : "deneme9", "lab" : "deneme9"})
    print(req.json())
    
    endpoint = "illness"
    url = "http://localhost:" + str(port) + "/"+ endpoint
    req = requests.get(url)
    print(req.json())

    endpoint = "illness/15:8"
    url = "http://localhost:" + str(port) + "/"+ endpoint
    req = requests.post(url, data = {"name" : "deneme6", "category" : "deneme6", "history" : "deneme6", "lab" : "deneme6"})
    print(req.json())

    endpoint = "illness/15:8"
    url = "http://localhost:" + str(port) + "/"+ endpoint
    req = requests.get(url)
    print(req.json())

    endpoint = "diagnosis"
    url = "http://localhost:" + str(port) + "/"+ endpoint
    req = requests.put(url, data = {"name" : "tanı9", "explanation" : "tanı9", "type" : "0", "linked_to" : "17:11", "illness_id" : "15:8"})
    print(req.json())

    endpoint = "diagnosis/18:9"
    url = "http://localhost:" + str(port) + "/"+ endpoint
    req = requests.get(url)
    print(req.json())

    endpoint = "diagnosis/18:9"
    url = "http://localhost:" + str(port) + "/"+ endpoint
    req = requests.post(url, data = {"name" : "tanı5", "explanation" : "tanı5", "type" : "1", "illness_id" : "15:8"})
    print(req.json())

    endpoint = "associated"
    url = "http://localhost:" + str(port) + "/"+ endpoint
    req = requests.get(url)
    print(req.json())

    endpoint = "associated/17:12"
    url = "http://localhost:" + str(port) + "/"+ endpoint
    req = requests.get(url)
    print(req.json())
main()