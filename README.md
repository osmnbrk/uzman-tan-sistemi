﻿ec2-18-196-227-115.eu-central-1.compute.amazonaws.com

# UZMAN TANI SİSTEMİ DOKÜMANTASYONU

## Endpointler

### GET \- /illness

Var olan tüm hastalık kayıtlarını getirir.

#### Örnek Link

`http://ec2-18-196-227-115.eu-central-1.compute.amazonaws.com:9797/illness`


### PUT\- /illness

Yeni hastalık kaydı oluşturur.

#### Örnek Link

`http://ec2-18-196-227-115.eu-central-1.compute.amazonaws.com:9797/illness`

Parametreler;
1) name
2) category
3) history
4) lab

### POST\- /illness/<illness_id>

Var olan hastalık kaydını günceller.

#### Örnek Link

`http://ec2-18-196-227-115.eu-central-1.compute.amazonaws.com:9797/illness/15:8`

Parametreler;
1) name
2) category
3) history
4) lab

### GET \- /illness/<illness_id>

İstenilen hastalığı getirir.

#### Örnek Link

`http://ec2-18-196-227-115.eu-central-1.compute.amazonaws.com:9797/illness/15:8`

### PUT\- /diagnosis

Yeni bir tanı kaydı oluşturur.

#### Örnek Link

`http://ec2-18-196-227-115.eu-central-1.compute.amazonaws.com:9797/diagnosis`

Parametreler;
1) name
2) explanation
3) type
4) linked_to
5) illness_id

### GET\- /diagnosis/<diagnosis_id>

İstenilen tanı kaydını getirir.

#### Örnek Link

`http://ec2-18-196-227-115.eu-central-1.compute.amazonaws.com:9797/diagnosis/18:20`

### POST\- /diagnosis/<diagnosis_id>

Var olan tanı kaydını günceller.

#### Örnek Link

`http://ec2-18-196-227-115.eu-central-1.compute.amazonaws.com:9797/diagnosis/18:20`

Parametreler;
1) name
2) explanation
3) type
4) illness_id

### GET\- /associated

Var olan tüm kayıtları ilişkisel olarak gruplu bir şekilde getirir.

#### Örnek Link

`http://ec2-18-196-227-115.eu-central-1.compute.amazonaws.com:9797/associated`

### GET\- /associated/<record_id>

İstenilen kaydın var olan tüm ilişkisel kayıtlarla getirir.

#### Örnek Link

`http://ec2-18-196-227-115.eu-central-1.compute.amazonaws.com:9797/associated/18:10`

### PUT\- /linker/<record_id>

İstenilen kayda yeni bir bağ oluşturur

#### Örnek Link

`http://ec2-18-196-227-115.eu-central-1.compute.amazonaws.com:9797/linker/18:10`

Parametreler;
1) linked_to

### DELETE\- /linker/<record_id>

İstenilen kaydın var olan bağını siler.

#### Örnek Link

`http://ec2-18-196-227-115.eu-central-1.compute.amazonaws.com:9797/linker/18:10`

Parametreler;
1) linked_to

### GET\- /category

Var olan kategorileri getirir.

#### Örnek Link

`http://ec2-18-196-227-115.eu-central-1.compute.amazonaws.com:9797/category`

### GET\- /user

Gönderilen emaile karşılık kullanıcı id'si döner.

#### Örnek Link

`http://ec2-18-196-227-115.eu-central-1.compute.amazonaws.com:9797/user`

Parametreler;
1) email

### POST\- /user

Kullanıcı girişi.

#### Örnek Link

`http://ec2-18-196-227-115.eu-central-1.compute.amazonaws.com:9797/user`

Parametreler;
1) email
2) password

### PUT\- /user

Kullanıcı girişi.

#### Örnek Link

`http://ec2-18-196-227-115.eu-central-1.compute.amazonaws.com:9797/user`

Parametreler;
1) name
2) surname
3) email
4) password
5) title

### POST\- /user/<user_id>

Kullanıcı şifresini günceller.

#### Örnek Link

`http://ec2-18-196-227-115.eu-central-1.compute.amazonaws.com:9797/user/20:11`

Parametreler;
1) password
