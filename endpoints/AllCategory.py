from flask_restful import reqparse, abort, Resource
from endpoints import DBHelper as db

class AllCategory(Resource):
    def get(self):
        client = db.create_connection()
        if client == False:
            return "Bağlanılamadı"
        category_list = db.all_category(client)
        db.close_connection(client)
        return category_list
    def post(self):
        pass
    def put(self):
        pass
    def delete(self):
        pass