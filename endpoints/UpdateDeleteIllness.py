from flask_restful import reqparse, abort, Resource
from endpoints import DBHelper as db

parser = reqparse.RequestParser()

parser.add_argument('name', type = str)
parser.add_argument('category', type = str)
parser.add_argument('history', type = str)
parser.add_argument('lab', type = str)

class UpdateDeleteIllness(Resource):
    def get(self, illness_id):
        args = parser.parse_args()
        client = db.create_connection()
        illness_dict = db.get_single_illness(illness_id, client)
        db.close_connection(client)
        return illness_dict
    def post(self, illness_id):
        args = parser.parse_args()
        client = db.create_connection()
        if args.name != '':
            update_name = db.update_illness_name(illness_id, args.name, client)
        if args.category != '':
            update_category = db.update_illness_category(illness_id, args.category, client)
        if args.history != '':
            update_history = db.update_illness_history(illness_id, args.history, client)
        if args.lab != '':
            update_lab = db.update_illness_lab(illness_id, args.lab, client)
        db.close_connection(client)
        if update_name != False or update_category != False or update_history != False or update_lab != False:
            return True
        else:
            return False
    def put(self):
        pass
    def delete(self):
        pass