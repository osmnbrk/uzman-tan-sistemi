from flask_restful import reqparse, abort, Resource
from endpoints import DBHelper as db

parser = reqparse.RequestParser()

parser.add_argument('name', type = str)
parser.add_argument('explanation', type = str)
parser.add_argument('type', type = str)
parser.add_argument('linked_to', type = str)
parser.add_argument('illness_id', type = str)

class NewDiagnosis(Resource):
    def get(self):
        pass
    def post(self):
        pass
    def put(self):
        args = parser.parse_args()
        client = db.create_connection()
        diagnosis_id = db.add_diagnosis(args.explanation, args.illness_id, args.name, args.type, client)
        db.create_link(args.linked_to, diagnosis_id, client)
        out_list = db.get_out_id(diagnosis_id, client)
        in_list = db.get_in_id(diagnosis_id, client)
        diagnosis_dict = {
            'id' : diagnosis_id,
            'out_links_id' : out_list,
            'in_links_id' : in_list
        }
        db.close_connection(client)
        return diagnosis_dict

    def delete(self):
        pass