from flask_restful import reqparse, abort, Resource
from endpoints import DBHelper as db

parser = reqparse.RequestParser()

parser.add_argument('password', type = str)

class UserUpdate(Resource):
    def get(self, user_id):
        args = parser.parse_args()
        client = db.create_connection()
        result = db.get_single_user(user_id, client)
        db.close_connection(client)
        return result
    def post(self, user_id):
        args = parser.parse_args()
        client = db.create_connection()
        result = db.update_user_password(user_id, args.password, client)
        db.close_connection(client)
        return result
    def put(self):
        pass
    def delete(self):
        pass