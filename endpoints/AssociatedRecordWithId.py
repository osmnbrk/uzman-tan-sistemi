from flask_restful import reqparse, abort, Resource
from endpoints import DBHelper as db

class AssociatedRecordWithId(Resource):
    def get(self, id):
        client = db.create_connection()
        result_dict = db.associated_records_with_id(id, client)
        db.close_connection(client)
        return result_dict
    def post(self):
        pass
    def put(self):
        pass
    def delete(self):
        pass