from flask_restful import reqparse, abort, Resource
from endpoints import DBHelper as db

parser = reqparse.RequestParser()

parser.add_argument('name', type = str)
parser.add_argument('category', type = str)
parser.add_argument('explanation', type = str)
parser.add_argument('type', type = str)
parser.add_argument('illness_id', type = str)

class UpdateDeleteDiagnosis(Resource):
    def get(self, diagnosis_id):
        client = db.create_connection()
        diagnosis_dict = db.get_single_diagnosis(diagnosis_id, client)
        db.close_connection(client)
        return diagnosis_dict
    def post(self, diagnosis_id):
        args = parser.parse_args()
        client = db.create_connection()
        result = db.update_diagnosis(diagnosis_id, args.name, args.illness_id, args.explanation, args.type, client)
        db.close_connection(client)
        return result
    def put(self):
        pass
    def delete(self):
        pass