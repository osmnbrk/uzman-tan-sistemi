from flask_restful import reqparse, abort, Resource
from endpoints import DBHelper as db

parser = reqparse.RequestParser()

parser.add_argument('linked_to', type = str)

class Linker(Resource):
    def get(self):
        pass
    def post(self):
        pass
    def put(self, id):
        args = parser.parse_args()
        client = db.create_connection()
        result = db.create_link(id, args.linked_to, client)
        db.close_connection(client)
        return result
    def delete(self):
        args = parser.parse_args()
        client = db.create_connection()
        result = db.delete_link(id, args.linked_to, client)
        db.close_connection(client)
        return result