from flask_restful import reqparse, abort, Resource
from endpoints import DBHelper as db

parser = reqparse.RequestParser()

parser.add_argument('name', type = str)
parser.add_argument('surname', type = str)
parser.add_argument('email', type = str)
parser.add_argument('password', type = str)
parser.add_argument('title', type = str)

class User(Resource):
    def get(self):
        args = parser.parse_args()
        client = db.create_connection()
        result = db.get_user_rid(args.email, client)
        db.close_connection(client)
        return result
    def post(self):
        args = parser.parse_args()
        client = db.create_connection()
        result = db.login(args.email, args.password, client)
        db.close_connection(client)
        return result
    def put(self):
        args = parser.parse_args()
        client = db.create_connection()
        result = db.add_user(args.name, args.surname, args.email, args.password, args.title, client)
        db.close_connection(client)
        return result
    def delete(self):
        pass