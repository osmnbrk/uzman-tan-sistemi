from flask_restful import reqparse, abort, Resource
from endpoints import DBHelper as db

class AssociatedRecord(Resource):
    def get(self):
        client = db.create_connection()
        result_list = db.associated_records(client)
        db.close_connection(client)
        return result_list
    def post(self, diagnosis_id):
        pass
    def put(self):
        pass
    def delete(self):
        pass