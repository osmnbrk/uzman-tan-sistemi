from flask_restful import reqparse, abort, Resource
from endpoints import DBHelper as db

parser = reqparse.RequestParser()

parser.add_argument('name', type = str)
parser.add_argument('category', type = str)
parser.add_argument('history', type = str)
parser.add_argument('lab', type = str)


class AllNewIllness(Resource):
    def get(self):
        args = parser.parse_args()
        client = db.create_connection()
        if client == False:
            return "Bağlanılamadı"
        illness_id = db.get_all_illness(client)
        db.close_connection(client)
        return illness_id
    def post(self):
        pass
    def put(self):
        args = parser.parse_args()
        client = db.create_connection()
        id_dict = db.add_illness(args.name, args.category, args.history, args.lab, client)
        db.close_connection(client)
        return id_dict
    def delete(self):
        pass