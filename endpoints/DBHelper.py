import pyorient

def create_connection():
    try:
        client = pyorient.OrientDB("localhost", 2424)
        client.db_open("uzman_tani_sistemi", "root", "rootpwd")
    except Exception as error:
        print(error)
        return False
    return client

def close_connection(client):
    try:
        client.db_close()
    except Exception as error:
        print(error)
        return False
    return True

def add_illness(name, category, history, lab, client):
    try:
        if check_illness_with_name(name, client) == True:
            return "This name already exists"
        else:
            query = "INSERT INTO illness (name, category, history, lab) VALUES ('" + name + "','" + category + "','" + history + "','" + lab + "')"
            result = client.command(query)
            if len(result) == 1:
                illness_rid = get_illness_rid(name, client)
                return illness_rid
            else:
                return False
    except Exception as error:
        print(error)
        return False
    
def check_illness_with_name(name, client):
    try:
        query = "SELECT COUNT(*) FROM illness WHERE name = '" + name + "'"
        result = client.command(query)
    except Exception as error:
        print(error)
        return False
    if result[0].COUNT != 0:
        return True
    else:
        return False

def check_illness_with_rid(rid, client):
    try:
        query = "SELECT COUNT(*) FROM illness WHERE @rid = " + rid
        result = client.command(query)
    except Exception as error:
        print(error)
        return False
    if result[0].COUNT != 0:
        return True
    else:
        return False

def get_all_illness(client):
    illness_list = []
    try:
        query = "SELECT FROM illness"
        result = client.command(query)
    except Exception as error:
        print(error)
        return False
    if len(result) == 0:
        return None
    else:
        for i in range(len(result)):
            temp_dict = {
                'id' : result[i]._OrientRecord__rid,
                'Name' : result[i].name,
                'Category' : result[i].category
            }
            illness_list.append(temp_dict)
    return illness_list

def get_illness_rid(illness_name, client):
    try:
        query = "select @rid from illness where name = '"+ illness_name +"'"
        result = client.command(query)
    except Exception as error:
        print(error)
        return False
    if len(result) == 0:
        return "Database is empty"
    return result[0].rid._OrientRecordLink__link

def get_illness_name(illness_rid, client):
    try:
        query = "SELECT name FROM illness WHERE @rid = " + illness_rid
        result = client.command(query)
    except Exception as error:
        print(error)
        return False
    if len(result) == 0:
        return "Database is empty"
    return result[0].name

def update_illness_name(rid, new_name, client):
    try:
        query = "UPDATE illness SET name = '" + new_name + "' WHERE @rid = " + rid
        result = client.command(query)
    except Exception as error:
        print(error)
        return False
    if len(result) == 1:
        return True
    else:
        return False

def update_illness_category(rid, new_category, client):
    try:
        query = "UPDATE illness SET category = '" + new_category + "' WHERE @rid = " + rid
        result = client.command(query)
    except Exception as error:
        print(error)
        return False
    if len(result) == 1:
        return True
    else:
        return False

def update_illness_history(rid, new_history, client):
    try:
        query = "UPDATE illness SET history = '" + new_history + "' WHERE @rid = " + rid
        result = client.command(query)
    except Exception as error:
        print(error)
        return False
    if len(result) == 1:
        return True
    else:
        return False

def update_illness_lab(rid, new_lab, client):
    try:
        query = "UPDATE illness SET lab = '" + new_lab + "' WHERE @rid = " + rid
        result = client.command(query)
    except Exception as error:
        print(error)
        return False
    if len(result) == 1:
        return True
    else:
        return False

def add_diagnosis(explanation, illness_id, name, ttype, client):
    try:
        query = "INSERT INTO diagnosis (explanation, illness_id, name, type) VALUES ('" + explanation + "','" + illness_id + "','" + name + "','" + ttype + "')"
        result = client.command(query)
        return result[0]._OrientRecord__rid
    except Exception as error:
        print(error)
        return False
    

def check_diagnosis_with_name(name, illness_id, client):
    try:
        query = "SELECT COUNT(*) FROM diagnosis WHERE name = '" + name + "' AND illness_id = " + illness_id 
        result = client.command(query)
    except Exception as error:
        print(error)
        return False
    if result[0].COUNT != 0:
        return True
    else:
        return False

def check_diagnosis_with_rid(rid, client):
    try:
        query = "SELECT COUNT(*) FROM diagnosis WHERE @rid = " + rid
        result = client.command(query)
    except Exception as error:
        print(error)
        return False
    if result[0].COUNT != 0:
        return True
    else:
        return False

def get_diagnosis_rid(diagnosis_name, illness_rid, client):
    try:
        query = "select from diagnosis where name = '"+ diagnosis_name +"' and illness_id = '" + illness_rid + "'"
        result = client.command(query)
        return result[0]._OrientRecord__rid
    except Exception as error:
        print(error)
        return False
    

def get_diagnosis_name(diagnosis_rid, client):
    try:
        query = "SELECT name FROM diagnosis WHERE @rid = " + diagnosis_rid
        result = client.command(query)
    except Exception as error:
        print(error)
        return False
    if len(result) == 0:
        return "Database is empty"
    return result[0].name

def create_link(source_rid, target_rid, client):
    try:
        query = "CREATE EDGE linker FROM " + source_rid + " TO " + target_rid
        result = client.command(query)
    except Exception as error:
        print(error)
        return False
    return True

def delete_link(source_rid, target_rid, client):
    try:
        query = "DELETE EDGE linker FROM "  + source_rid + " TO " + target_rid
        result = client.command(query)
    except Exception as error:
        print(error)
        return False
    return True

def update_diagnosis(diagnosis_rid, name, illness_id, explanation, type, client):
    try:
        query = "UPDATE diagnosis SET name = '" + name + "', illness_id = " + illness_id + " ,explanation = '" + explanation + "',type = '" + type +"' WHERE @rid =  "+ diagnosis_rid
        result = client.command(query)
    except Exception as error:
        print(error)
        return False
    if len(result) == 1:
        return True
    else:
        return False

def get_single_illness(illness_rid, client):
    try:
        if type(illness_rid) != str:
            illness_rid = str(illness_rid)
        query = "SELECT name, category, history, lab FROM illness WHERE @rid = " + illness_rid
        result = client.command(query)
        result_in = get_in_id(illness_rid, client)
        result_out = get_out_id(illness_rid, client)
        illness_dict = {
            'id' : illness_rid,
            'name' : result[0].name,
            'category' : result[0].category,
            'history' : result[0].history,
            'lab' : result[0].lab,
            'in_links' : result_in,
            'out_links' : result_out
        }
    except Exception as error:
        print(error)
        return False
    if len(result) == 0:
        return None
    return illness_dict

def get_in_id(rid, client):
    out_list = []
    try:
        query = "SELECT FROM E WHERE in = " + rid
        result = client.command(query)
        for i in range(len(result)):
            out_dict = {}
            out_dict['id'] = result[i]._OrientRecord__o_storage['out']._OrientRecordLink__link
            out_list.append(out_dict)

        return out_list
    except Exception as error:
        print(error)
        return False

def get_out_id(rid, client):
    in_list = []
    try:
        query = "SELECT FROM E WHERE out = " + rid
        result = client.command(query)
        for i in range(len(result)):
            in_dict = {}
            in_dict['name'] = result[i]._OrientRecord__o_storage['in']._OrientRecordLink__link
            in_list.append(in_dict)
        return in_list
    except Exception as error:
        print(error)
        return False

def get_single_diagnosis(diagnosis_rid, client):
    try:
        query = "SELECT name, illness_id, explanation, type FROM diagnosis WHERE @rid = '" + diagnosis_rid + "'"
        result = client.command(query)
        out_list = get_out_id(diagnosis_rid, client)
        in_list = get_in_id(diagnosis_rid, client)
        diagnosis_dict = {
            'id' : diagnosis_rid,
            'name' : result[0].name,
            'illness_id' : result[0].illness_id,
            'explanation' : result[0].explanation,
            'type' : result[0].type,
            'out_links_id' : out_list,
            'in_links_id' : in_list
        }
    except Exception as error:
        print(error)
        return False
    if len(result) == 0:
        return "Database is empty"
    return diagnosis_dict

"""def get_all_diagnosis(client):
    diagnosis_list = []
    try:
        query = "SELECT FROM illness"
        result = client.command(query)
    except Exception as error:
        print(error)
        return False
    if len(result) == 0:
        return None
    else:
        for i in range(len(result)):
            temp_dict = {
                '@rid' : result[i]._OrientRecord__rid,
                'Name' : result[i].name
            }
            illness_list.append(temp_dict)
    return illness_list"""

def associated_records(client):
    try:
        illness_list = get_all_illness(client)
        for i in range(len(illness_list)):
            try:
                illness_list[i]['diagnosises'] = []
                query = "SELECT @rid, name FROM diagnosis WHERE illness_id = '" + illness_list[i]['id'] + "'"
                result = client.command(query)
                for j in range(len(result)):
                    diagnosis_id = result[j].rid._OrientRecordLink__link
                    out_list = get_out_id(diagnosis_id, client)
                    in_list = get_in_id(diagnosis_id, client)
                    diagnosis_dict = {
                        'id' : diagnosis_id,
                        'name' : result[j].name,
                        'out_links_id' : out_list,
                        'in_links_id' : in_list
                    }
                    illness_list[i]['diagnosises'].append(diagnosis_dict)
            except Exception as error:
                print(error)
                return False
    except Exception as error:
        print(error)
        return False
    return illness_list

def associated_records_with_id(id, client):
    try:
        if check_illness_with_rid(id, client) == True:
            illness_dict = get_single_illness(id, client)
            try:
                illness_dict['diagnosises'] = []
                query = "SELECT @rid, name, type, explanation FROM diagnosis WHERE illness_id = '#" + illness_dict['id'] + "'"
                result = client.command(query)
                for j in range(len(result)):
                    diagnosis_id = result[j].rid._OrientRecordLink__link
                    out_list = get_out_id(diagnosis_id, client)
                    in_list = get_in_id(diagnosis_id, client)
                    diagnosis_dict = {
                        'id' : diagnosis_id,
                        'name' : result[j].name,
                        'type' : result[j].type,
                        'explanation' : result[j].explanation,
                        'out_links_id' : out_list,
                        'in_links_id' : in_list
                    }
                    illness_dict['diagnosises'].append(diagnosis_dict)
            except Exception as error:
                print(error)
                return False
        elif check_diagnosis_with_rid(id, client) == True:
            """temp_dict = get_single_diagnosis(id, client)
            illness_id = temp_dict['illness_id']
            illness_dict = get_single_illness(illness_id, client)
            try:
                illness_dict['diagnosises'] = []
                query = "SELECT @rid, name, type FROM diagnosis WHERE illness_id = '#" + illness_dict['id'] + "'"
                result = client.command(query)
                for j in range(len(result)):
                    diagnosis_id = result[j].rid._OrientRecordLink__link
                    out_list = get_out_id(diagnosis_id, client)
                    in_list = get_in_id(diagnosis_id, client)
                    diagnosis_dict = {
                        'id' : diagnosis_id,
                        'name' : result[j].name,
                        'type' : result[j].type,
                        'out_links_id' : out_list,
                        'in_links_id' : in_list
                    }
                    illness_dict['diagnosises'].append(diagnosis_dict)
            except Exception as error:
                print(error)
                return False"""
            diagnosis_dict = {}
            out_links = get_out_id(id, client)
            query = "SELECT @rid, name, type, explanation FROM diagnosis WHERE @rid = '#" + id + "'"
            result = client.command(query)
            diagnosis_dict['id'] = id
            diagnosis_dict['name'] = result[0].name
            diagnosis_dict['type'] = result[0].type
            diagnosis_dict['explanation'] = result[0].explanation
            diagnosis_dict['below_diagnosis'] = []
            for i in range(len(out_links)):
                temp_dict = {}
                temp_id = out_links[i]['name']
                query = "SELECT @rid, name, type, explanation FROM diagnosis WHERE @rid = '#" + temp_id + "'"
                result = client.command(query)
                temp_dict['id'] = temp_id
                temp_dict['name'] = result[0].name
                temp_dict['type'] = result[0].type
                temp_dict['explanation'] = result[0].explanation
                diagnosis_dict['below_diagnosis'].append(temp_dict)
            return diagnosis_dict
        else:
            return False
    except Exception as error:
        print(error)
        return False
    return illness_dict

def all_category(client):
    try:
        query = "SELECT name FROM category_table order by name"
        result = client.command(query)
        category_list = []
        for i in range(len(result)):
            category_dict = {}
            temp = result[i]._OrientRecord__o_storage['name']
            category_dict['name'] = temp
            category_list.append(category_dict)
    except Exception as error:
        print(error)
        return False
    if len(result) == 0:
        return None
    return category_list

def add_user(name, surname, email, password, title, client):
    try:
        query = "INSERT INTO user (name, surname, email, password, title) VALUES ('" + name + "','" + surname + "','" + email + "','" + password + "','" + title + "')"
        result = client.command(query)
        if len(result) == 1:
            return True
        else:
            return False
    except Exception as error:
        print(error)
        return False

def login(email, password, client):
    try:
        query = "SELECT COUNT(*) FROM user WHERE email = '" + email + "' AND password = '" + password + "'" 
        result = client.command(query)
    except Exception as error:
        print(error)
        return False
    if result[0].COUNT != 0:
        return True
    else:
        return False

def update_user_password(rid, new_password, client):
    try:
        query = "UPDATE user SET password = '" + new_password + "' WHERE @rid = " + rid
        result = client.command(query)
    except Exception as error:
        print(error)
        return False
    if len(result) == 1:
        return True
    else:
        return False

def get_user_rid(user_email, client):
    try:
        query = "select @rid from user where email = '"+ user_email +"'"
        result = client.command(query)
    except Exception as error:
        print(error)
        return False
    if len(result) == 0:
        return "Database is empty"
    return result[0].rid._OrientRecordLink__link

def get_single_user(user_id, client):
    try:
        if type(user_id) != str:
            user_id = str(user_id)
        query = "SELECT name, surname, email, password, title FROM user WHERE @rid = " + user_id
        result = client.command(query)
        user_dict = {
            'id' : user_id,
            'name' : result[0].name,
            'surname' : result[0].surname,
            'email' : result[0].email,
            'password' : result[0].password,
            'title' : result[0].title
        }
    except Exception as error:
        print(error)
        return False
    if len(result) == 0:
        return None
    return user_dict