from flask import Flask
from flask_restful import reqparse, abort, Api, Resource

from endpoints import AllNewIllness
from endpoints import UpdateDeleteIllness
from endpoints import NewDiagnosis
from endpoints import UpdateDeleteDiagnosis
from endpoints import AssociatedRecord
from endpoints import AssociatedRecordWithId
from endpoints import Linker
from endpoints import AllCategory
from endpoints import User
from endpoints import UserUpdate

app = Flask(__name__)
api = Api(app)

api.add_resource(AllNewIllness.AllNewIllness, '/illness')#Çalışıyor
api.add_resource(UpdateDeleteIllness.UpdateDeleteIllness, '/illness/<illness_id>')#Çalışıyor
api.add_resource(NewDiagnosis.NewDiagnosis, '/diagnosis')#Çalışıyor
api.add_resource(UpdateDeleteDiagnosis.UpdateDeleteDiagnosis, '/diagnosis/<diagnosis_id>')#Çalışıyor
api.add_resource(AssociatedRecord.AssociatedRecord, '/associated')#Çalışıyor
api.add_resource(AssociatedRecordWithId.AssociatedRecordWithId, '/associated/<id>')#Çalışıyor
api.add_resource(Linker.Linker, '/link/<id>')#Çalışıyor
api.add_resource(AllCategory.AllCategory, '/category')#Çalışıyor
api.add_resource(User.User, '/user')#Çalışıyor
api.add_resource(UserUpdate.UserUpdate, '/user/<user_id>')#Çalışıyor

if __name__ == '__main__':
    app.run(debug=False, host = '0.0.0.0', port = 9797)
