HastalikListe 
post : (parametresiz)
return : (hastalik_id, Name)

HastalikDetay
post : (hastalik_id)
return : Hastalık Detay bilgileri tüm alanlar (id, Name, Öykü, Lab, vs.)

TaniListe
post : (parametresiz)
return : (tani_id, name, linked_to)

TaniDetay
post : (tani_id)
return : Tanı Detay bilgileri tüm alanlar (id, Name, linked_to, type, explanation vs.)

TumKayitlar
post : (parametresiz)
return : (hastalik_id, Name, tani_id, Name, linked_to)

HastalikKayit 
post (parametreleri sen seç) - ((Name, Öykü, Lab))
return : (hastalik_id)

TaniKayit 
post (parametreleri sen seç) - (Name, linked_to, type, explanation vs.)
return : (tani_id, linked_to)

HastalikGuncelle 
post where (hastalik_id)
post value tüm alanlar (id, Name, Öykü, Lab, vs.)
return : (1/0)

TaniGuncelle
post where (tani_id)
post value Tanı Detay bilgileri tüm alanlar (id, Name, linked_to, type, explanation vs.)
return : (1/0)

