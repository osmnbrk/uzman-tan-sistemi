<?php
session_start();
if($_SESSION["email"]!='' && $_SESSION["title"]==1 ){
?>
<!DOCTYPE html>
<html>
<title>Hastalık Kaydet - Uzman Tanı Sistemi</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="uts.css">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Roboto'>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.5/css/bulma.min.css" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">



<body class="w3-light-grey">
  <nav class="navbar navbar-expand-lg navbar-light bg-purple sticky-top ">
    <a class="navbar-brand " href="AnaSayfa.php">Uzman Tanı Sistemi</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarText">
      <ul class="navbar-nav mr-auto">

      </ul>
      <span class="navbar-text">
        <div class="btn-group">
          <button type="button" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <svg class="bi bi-person-fill" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
              <path fill-rule="evenodd" d="M3 14s-1 0-1-1 1-4 6-4 6 3 6 4-1 1-1 1H3zm5-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6z" />
            </svg>
          </button>
          <div class="dropdown-menu dropdown-menu-right">
            <a class="dropdown-item" href="Ayarlar.php">Ayarlar</a>
            <a class="dropdown-item" href="Cikis.php">Çıkış Yap</a>
          </div>
        </div>
      </span>
    </div>
  </nav>

  <!-- Page Container -->
  <div class="w3-content w3-margin-top" style="max-width:1400px;">


    <!-- The Grid -->
    <div class="w3-row-padding">

      <!-- Left Column -->
      <div class="w3-third col-md-3">

        <div class="w3-white w3-text-grey w3-card-4">
          <div class="w3-display-container">
            <br><br>
            <br>
            <div class="w3-display-bottomleft w3-container w3-text-black">
              <h5 class="text-secondary"> Hastalık Kaydı Açma <hr></h5>
            </div>
          </div>


          <div class="w3-container">
            <div class="form-group">
              <form action="">

                <p class="w3-large w3-text-theme"><b>Hastalık Adı:</b></p>
                <textarea class="form-control" type="text" id="name" name="name" value=""></textarea><br>

                <p class="w3-large w3-text-theme"><b>Kategori:</b></p>
                <select class="test" id="category" name="category" multiple="multiple">

                  <option value="Acil/Yoğun Bakım">Acil/Yoğun Bakım</option>
                  <option value="Adölesan">Adölesan</option>
                  <option value="Alerji">Alerji</option>
                  <option value="Çocuk Cerrahisi">Çocuk Cerrahisi</option>
                  <option value="Çocuk Psikiyatrisi">Çocuk Psikiyatrisi</option>
                  <option value="Deri Hastalıkları/Dermatoloji">Deri Hastalıkları/Dermatoloji</option>
                  <option value="Endokrinoloji">Endokrinoloji</option>
                  <option value="Enfeksiyon">Enfeksiyon</option>
                  <option value="Gastrointestinal Sistem">Gastrointestinal Sistem</option>
                  <option value="Genetik">Genetik</option>
                  <option value="Göz Hastalıkları">Göz Hastalıkları</option>
                  <option value="Hematoloji/Onkoloji">Hematoloji/Onkoloji</option>
                  <option value="Kardiyoloji">Kardiyoloji</option>
                  <option value="Kulak Burun Boğaz">Kulak Burun Boğaz</option>
                  <option value="Metabolizma">Metabolizma</option>
                  <option value="Nefroloji">Nefroloji</option>
                  <option value="Nöroloji">Nöroloji</option>
                  <option value="Ortopedi">Ortopedi</option>
                  <option value="Romatoloji">Romatoloji</option>
                  <option value="Solunum Sistemi">Solunum Sistemi</option>
                  <option value="Ürogenital Sistem">Ürogenital Sistem</option>
                  <option value="Yenidoğan">Yenidoğan</option>

                </select>

                <p class="w3-large w3-text-theme"><b>Öyküsü:</b></p>
                <textarea class="form-control" type="text" id="history" name="history" value=""></textarea><br>

                <p class="w3-large w3-text-theme"><b>Lab:</b></p>
                <textarea class="form-control" type="text" id="lab" name="lab" value=""></textarea><br>

              </form>
            </div>

            <div class="btn bg-purple text-light" type="button" id="kaydet" class="button is-small is-link">Kaydet</div>


            <br>
            <div class="clearfix"></div>
            <div id="SonucYaz"></div>



            <br>
          </div>
        </div><br>

        <!-- End Left Column -->
      </div>

      <!-- Right Column -->
      <div class="w3-twothird col-md-9">

        <div class="w3-container w3-card w3-white w3-margin-bottom">
          <h5 class="w3-text-grey w3-padding-16"><i class="fa fa-list fa-fw w3-margin-right p-2"></i>Kayıtlı Hastalıklar   <hr></h5>

          <div class="w3-container">


            <div id="HastalikListe">


            </div>

          </div>
        </div>
        <br>

      </div>
      <!-- End Right Column -->
    </div>

    <!-- End Grid -->
  </div>

  <!-- End Page Container -->
  </div>

  <footer class="w3-container w3-teal w3-center w3-margin-top bg-purple">
    <p>Uludağ Üniversitesi Bilgisayar Mühendisliği Bitirme Projesi</p>

    <p> <a href="" target="_blank">GitHub</a></p>
  </footer>

</body>

</html>
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

<script type="text/javascript">
  $(document).ready(function() {



    $.ajax({
      url: "HastalikGetirApi.php",
      type: "GET", // #ya da POST

      beforeSend: function() {
        $("#HastalikListe").html('<div style="font-size:11px;" title="Hastalıklar Yükleniyor" >Hastalıklar yükleniyor...! <i class="fa fa-refresh fa-spin" style="font-size:14px"></i></div>');
      },

      success: function(hastaliklar) {

        $("#HastalikListe").html(hastaliklar);

        console.log(hastaliklar);

      },
      error: function(error) {
        //  $("#district").html(error);
        console.log(`Error ${error}`);
      }
    });







    //  const endPoint = "KaydetApi.php";
    $('#kaydet').click(function() {


      var name = $("#name").val();
      var category = $("#category").val();
      var history = $("#history").val();
      var lab = $("#lab").val();

      console.log(history);

      if (name == '' || category == '' || history=="") {
        $('#SonucYaz').html('<span class="text-danger">Boş Alanları Doldurunuz...!</span>');

      } else {


        $.ajax({
          url: "KaydetApi.php",
          type: "POST", // #ya da POST
          //  data : $("form").serializeArray(),
          data: {
            name: name,
            category: category,
            history: history,
            lab: lab
          },

          beforeSend: function() {
            $("#kaydet").html('<div style="font-size:11px;" title="Lütfen Bekleyiniz" >Lütfen Bekleyiniz...! <i class="fa fa-refresh fa-spin" style="font-size:14px"></i></div>');
            $("#kaydet").attr("disabled", true);
          },

          success: function(response) {

              $('#SonucYaz').html(response);



            $("#kaydet").html('Kaydet');
            $("#kaydet").attr("disabled", false);





            $.ajax({
              url: "HastalikGetirApi.php",
              type: "GET", // #ya da POST

              beforeSend: function() {
                $("#HastalikListe").html('<div style="font-size:11px;" title="Hastalıklar Yükleniyor" >Hastalıklar yükleniyor...! <i class="fa fa-refresh fa-spin" style="font-size:14px"></i></div>');
              },

              success: function(hastaliklar) {

                $("#HastalikListe").html(hastaliklar);

                console.log(hastaliklar);

              },
              error: function(error) {
                //  $("#district").html(error);
                console.log(`Error ${error}`);
              }
            });
            console.log(response);

          },
          error: function(error) {
            //  $("#district").html(error);
            console.log(`Error ${error}`);
          }
        });
      }
    });
  });
</script>


<script type="text/javascript">
  (function($) {

    $.fn.fSelect = function(options) {

      if (typeof options == 'string') {
        var settings = options;
      } else {
        var settings = $.extend({
          placeholder: 'Select some options',
          numDisplayed: 3,
          overflowText: '{n} selected',
          searchText: 'Search',
          showSearch: true
        }, options);
      }


      /**
       * Constructor
       */
      function fSelect(select, settings) {
        this.$select = $(select);
        this.settings = settings;
        this.create();
      }


      /**
       * Prototype class
       */
      fSelect.prototype = {
        create: function() {
          this.settings.multiple = this.$select.is('[multiple]');
          var multiple = this.settings.multiple ? ' multiple' : '';
          this.$select.wrap('<div class="fs-wrap' + multiple + '" tabindex="0"></div>');
          this.$select.before('<div class="fs-label-wrap"><div class="fs-label">' + this.settings.placeholder + '</div><span class="fs-arrow"></span></div>');
          this.$select.before('<div class="fs-dropdown hidden"><div class="fs-options"></div></div>');
          this.$select.addClass('hidden');
          this.$wrap = this.$select.closest('.fs-wrap');
          this.$wrap.data('id', window.fSelect.num_items);
          window.fSelect.num_items++;
          this.reload();
        },

        reload: function() {
          if (this.settings.showSearch) {
            var search = '<div class="fs-search"><input type="search" placeholder="' + this.settings.searchText + '" /></div>';
            this.$wrap.find('.fs-dropdown').prepend(search);
          }
          this.selected = [].concat(this.$select.val()); // force an array
          var choices = this.buildOptions(this.$select);
          this.$wrap.find('.fs-options').html(choices);
          this.reloadDropdownLabel();
        },

        destroy: function() {
          this.$wrap.find('.fs-label-wrap').remove();
          this.$wrap.find('.fs-dropdown').remove();
          this.$select.unwrap().removeClass('hidden');
        },

        buildOptions: function($element) {
          var $this = this;

          var choices = '';
          $element.children().each(function(i, el) {
            var $el = $(el);

            if ('optgroup' == $el.prop('nodeName').toLowerCase()) {
              choices += '<div class="fs-optgroup">';
              choices += '<div class="fs-optgroup-label">' + $el.prop('label') + '</div>';
              choices += $this.buildOptions($el);
              choices += '</div>';
            } else {
              var val = $el.prop('value');

              // exclude the first option in multi-select mode
              if (0 < i || '' != val || !$this.settings.multiple) {
                var disabled = $el.is(':disabled') ? ' disabled' : '';
                var selected = -1 < $.inArray(val, $this.selected) ? ' selected' : '';
                choices += '<div class="fs-option' + selected + disabled + '" data-value="' + val + '"><span class="fs-checkbox"><i></i></span><div class="fs-option-label">' + $el.html() + '</div></div>';
              }
            }
          });

          return choices;
        },

        reloadDropdownLabel: function() {
          var settings = this.settings;
          var labelText = [];

          this.$wrap.find('.fs-option.selected').each(function(i, el) {
            labelText.push($(el).find('.fs-option-label').text());
          });

          if (labelText.length < 1) {
            labelText = settings.placeholder;
          } else if (labelText.length > settings.numDisplayed) {
            labelText = settings.overflowText.replace('{n}', labelText.length);
          } else {
            labelText = labelText.join(', ');
          }

          this.$wrap.find('.fs-label').html(labelText);
          this.$select.change();
        }
      }


      /**
       * Loop through each matching element
       */
      return this.each(function() {
        var data = $(this).data('fSelect');

        if (!data) {
          data = new fSelect(this, settings);
          $(this).data('fSelect', data);
        }

        if (typeof settings == 'string') {
          data[settings]();
        }
      });
    }


    /**
     * Events
     */
    window.fSelect = {
      'num_items': 0,
      'active_id': null,
      'active_el': null,
      'last_choice': null,
      'idx': -1
    };

    $(document).on('click', '.fs-option:not(.disabled)', function(e) {
      var $wrap = $(this).closest('.fs-wrap');
      var do_close = false;

      if ($wrap.hasClass('multiple')) {
        var selected = [];

        // shift + click support
        if (e.shiftKey && null != window.fSelect.last_choice) {
          var current_choice = parseInt($(this).attr('data-index'));
          var do_select = !$(this).hasClass('selected');
          var min = Math.min(window.fSelect.last_choice, current_choice);
          var max = Math.max(window.fSelect.last_choice, current_choice);

          for (i = min; i <= max; i++) {
            $wrap.find('.fs-option[data-index=' + i + ']')
              .not('.hidden, .disabled')
              .each(function() {
                do_select ?
                  $(this).addClass('selected') :
                  $(this).removeClass('selected');
              });
          }
        } else {
          window.fSelect.last_choice = parseInt($(this).attr('data-index'));
          $(this).toggleClass('selected');
        }

        $wrap.find('.fs-option.selected').each(function(i, el) {
          selected.push($(el).attr('data-value'));
        });
      } else {
        var selected = $(this).attr('data-value');
        $wrap.find('.fs-option').removeClass('selected');
        $(this).addClass('selected');
        do_close = true;
      }

      $wrap.find('select').val(selected);
      $wrap.find('select').fSelect('reloadDropdownLabel');

      if (do_close) {
        closeDropdown($wrap);
      }
    });

    $(document).on('keyup', '.fs-search input', function(e) {
      if (40 == e.which) { // down
        $(this).blur();
        return;
      }

      var $wrap = $(this).closest('.fs-wrap');
      var matchOperators = /[|\\{}()[\]^$+*?.]/g;
      var keywords = $(this).val().replace(matchOperators, '\\$&');

      $wrap.find('.fs-option, .fs-optgroup-label').removeClass('hidden');

      if ('' != keywords) {
        $wrap.find('.fs-option').each(function() {
          var regex = new RegExp(keywords, 'gi');
          if (null === $(this).find('.fs-option-label').text().match(regex)) {
            $(this).addClass('hidden');
          }
        });

        $wrap.find('.fs-optgroup-label').each(function() {
          var num_visible = $(this).closest('.fs-optgroup').find('.fs-option:not(.hidden)').length;
          if (num_visible < 1) {
            $(this).addClass('hidden');
          }
        });
      }

      setIndexes($wrap);
    });

    $(document).on('click', function(e) {
      var $el = $(e.target);
      var $wrap = $el.closest('.fs-wrap');

      if (0 < $wrap.length) {

        // user clicked another fSelect box
        if ($wrap.data('id') !== window.fSelect.active_id) {
          closeDropdown();
        }

        // fSelect box was toggled
        if ($el.hasClass('fs-label') || $el.hasClass('fs-arrow')) {
          var is_hidden = $wrap.find('.fs-dropdown').hasClass('hidden');

          if (is_hidden) {
            openDropdown($wrap);
          } else {
            closeDropdown($wrap);
          }
        }
      }
      // clicked outside, close all fSelect boxes
      else {
        closeDropdown();
      }
    });

    $(document).on('keydown', function(e) {
      var $wrap = window.fSelect.active_el;
      var $target = $(e.target);

      // toggle the dropdown on space
      if ($target.hasClass('fs-wrap')) {
        if (32 == e.which) {
          $target.find('.fs-label').trigger('click');
          return;
        }
      }
      // preserve spaces during search
      else if (0 < $target.closest('.fs-search').length) {
        if (32 == e.which) {
          return;
        }
      } else if (null === $wrap) {
        return;
      }

      if (38 == e.which) { // up
        e.preventDefault();

        $wrap.find('.fs-option').removeClass('hl');

        if (window.fSelect.idx > 0) {
          window.fSelect.idx--;
          $wrap.find('.fs-option[data-index=' + window.fSelect.idx + ']').addClass('hl');
          setScroll($wrap);
        } else {
          window.fSelect.idx = -1;
          $wrap.find('.fs-search input').focus();
        }
      } else if (40 == e.which) { // down
        e.preventDefault();

        var last_index = $wrap.find('.fs-option:last').attr('data-index');
        if (window.fSelect.idx < parseInt(last_index)) {
          window.fSelect.idx++;
          $wrap.find('.fs-option').removeClass('hl');
          $wrap.find('.fs-option[data-index=' + window.fSelect.idx + ']').addClass('hl');
          setScroll($wrap);
        }
      } else if (32 == e.which || 13 == e.which) { // space, enter
        e.preventDefault();

        $wrap.find('.fs-option.hl').click();
      } else if (27 == e.which) { // esc
        closeDropdown($wrap);
      }
    });

    function setIndexes($wrap) {
      $wrap.find('.fs-option:not(.hidden,.disabled)').each(function(i, el) {
        $(el).attr('data-index', i);
        $wrap.find('.fs-option').removeClass('hl');
      });
      $wrap.find('.fs-search input').focus();
      window.fSelect.idx = -1;
    }

    function setScroll($wrap) {
      var $container = $wrap.find('.fs-options');
      var $selected = $wrap.find('.fs-option.hl');

      var itemMin = $selected.offset().top + $container.scrollTop();
      var itemMax = itemMin + $selected.outerHeight();
      var containerMin = $container.offset().top + $container.scrollTop();
      var containerMax = containerMin + $container.outerHeight();

      if (itemMax > containerMax) { // scroll down
        var to = $container.scrollTop() + itemMax - containerMax;
        $container.scrollTop(to);
      } else if (itemMin < containerMin) { // scroll up
        var to = $container.scrollTop() - containerMin - itemMin;
        $container.scrollTop(to);
      }
    }

    function openDropdown($wrap) {
      window.fSelect.active_el = $wrap;
      window.fSelect.active_id = $wrap.data('id');
      window.fSelect.initial_values = $wrap.find('select').val();
      $wrap.find('.fs-dropdown').removeClass('hidden');
      setIndexes($wrap);
    }

    function closeDropdown($wrap) {
      if ('undefined' == typeof $wrap && null != window.fSelect.active_el) {
        $wrap = window.fSelect.active_el;
      }
      if ('undefined' !== typeof $wrap) {
        // only trigger if the values have changed
        var initial_values = window.fSelect.initial_values;
        var current_values = $wrap.find('select').val();
        if (JSON.stringify(initial_values) != JSON.stringify(current_values)) {
          $(document).trigger('fs:changed', $wrap);
        }
      }

      $('.fs-dropdown').addClass('hidden');
      window.fSelect.active_el = null;
      window.fSelect.active_id = null;
      window.fSelect.last_choice = null;
    }

  })(jQuery);

  $(document).ready(function() {
    $('.test').fSelect({
      forceCustomRendering: true
    });
  });
</script>
<?php
}else{
echo '<script type="text/javascript">
      alert("Yetkiniz Yok");
     window.location = "AnaSayfa.php"
</script>';

}
?>
