<?php ob_start();?>
<!doctype html>
<html lang="tr">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="userislemleri.css">

    <title>Giris - Uzman Tanı Sistemi</title>
  </head>
  <body>
    <div class="vid-container">

  <div class="inner-container">

    <div class="box col">
      <h1 class="text-dark">Giriş</h1>
      <input type="email"  name="email" id="email" placeholder="E-mail"/>
      <input type="password" name="password" id="password"  placeholder="Şifre"/>
      <button id="giris" onclick="Giris()">Giriş</button>
      <div id="uyari">

      </div>
      <p><a href="SifremiUnuttum.php">Şifrenizi mi unutunuz?</a><a href="KaydolYeni.php">Kaydol</a></p>
      <div id="SonucYaz">

      </div>
    </div>
  </div>
</div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" ></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

  </body>
</html>


<script type="text/javascript">
  function Giris() {


    var email = $("#email").val();
    var password = $("#password").val();



    if (email == '' || password == "") {
      $('#uyari').html('<span class="text-danger">Boş Alanları Doldurunuz...!</span>');

    } else {


      $.ajax({
        url: "GirisApi.php",
        type: 'POST', // #ya da POST
        //  data : $("form").serializeArray(),
        data: {
          email: email,
          password: password,
        },
        beforeSend: function() {
          $("#giris").html('<div style="font-size:16px;" title="Giriş yapılıyor " >Giriş yapılıyor <i class="fa fa-refresh fa-spin" style="font-size:16px"></i></div>');
          $("#giris").attr("disabled", true);
        },

        success: function(response) {

          $("#giris").html('Giriş');
          $("#giris").attr("disabled", false);

          if (response == 2) {

            alert('Hatalı Şifre');

            $("#email").val('');
            $("#password").val('');
            $("#uyari").html('');

          } else {

            $("#uyari").html(response);
          }
          

          $("#email").val();
          $("#password").val();

          // if (response == 1) {
          //      exit;
          //      foo;
          //
          // }




          console.log(response);

        },
        error: function(error) {
          //  $("#district").html(error);
          console.log(`Error ${error}`);
        }
      });
    }
  };
</script>
