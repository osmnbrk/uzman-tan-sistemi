<?php
session_start();
if($_SESSION["email"]!=''){
?>
<!doctype html>
<html lang="tr">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="uts.css">

  <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
  <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Roboto'>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.5/css/bulma.min.css" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

  <title>Anasayfa - Uzman Tanı Sistemi</title>
</head>


<body>
  <nav class="navbar navbar-expand-lg navbar-light bg-purple  sticky-top ">
    <a class="navbar-brand " href="AnaSayfa.php">Uzman Tanı Sistemi</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarText">
      <ul class="navbar-nav mr-auto">

      </ul>
      <span class="navbar-text">
        <div class="btn-group">
          <?php echo $_SESSION["name"];?>
          <button type="button" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <svg class="bi bi-person-fill" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
              <path fill-rule="evenodd" d="M3 14s-1 0-1-1 1-4 6-4 6 3 6 4-1 1-1 1H3zm5-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6z" />
            </svg>
          </button>
          <div class="dropdown-menu dropdown-menu-right">
            <a class="dropdown-item" href="Ayarlar.php">Ayarlar</a>
            <a class="dropdown-item" href="Cikis.php">Çıkış Yap</a>
          </div>
        </div>
      </span>
    </div>
  </nav>

  <div class="container">

    <p></p>

    <div id="HastalikListe">

    </div>

  </div>

  <footer class="w3-container w3-teal w3-center w3-margin-top bg-purple">
    <p>Uludağ Üniversitesi Bilgisayar Mühendisliği Bitirme Projesi</p>

    <p> <a href="" target="_blank">GitHub</a></p>
  </footer>

  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
</body>

</html>
<script type="text/javascript">
  $(document).ready(function() {

    // sayfa yüklenirken hastalıkları açılır pencereye yükler
    $.ajax({
      url: "HastalikGetirApi.php",
      type: "GET", // #ya da POST

      beforeSend: function() {
        $("#HastalikListe").html('<div style="font-size:11px;" title="Hastalıklar Yükleniyor" >Hastalıklar yükleniyor...! <i class="fa fa-refresh fa-spin" style="font-size:14px"></i></div>');
      },

      success: function(hastaliklar) {

        $("#HastalikListe").html(hastaliklar);

        console.log(hastaliklar);

      },
      error: function(error) {
        //  $("#district").html(error);
        console.log(`Error ${error}`);
      }
    });
  });
</script>
<?php
}else{
echo '<script type="text/javascript">
     window.location = "GirisYap.php"
</script>';

}
?>
