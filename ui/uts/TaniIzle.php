<?php
session_start();
if($_SESSION["email"]!=''){
?>
<!DOCTYPE html>
<html>
<title>Tani İzle - Uzman Tanı Sistemi</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="uts.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Roboto'>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.5/css/bulma.min.css" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

<body class="w3-light-grey">
  <nav class="navbar navbar-expand-lg navbar-light bg-purple sticky-top">
    <a class="navbar-brand " href="AnaSayfa.php">Uzman Tanı Sistemi</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarText">
      <ul class="navbar-nav mr-auto">

      </ul>
      <span class="navbar-text">
        <div class="btn-group">
          <button type="button" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <svg class="bi bi-person-fill" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
              <path fill-rule="evenodd" d="M3 14s-1 0-1-1 1-4 6-4 6 3 6 4-1 1-1 1H3zm5-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6z" />
            </svg>
          </button>
          <div class="dropdown-menu dropdown-menu-right">
            <a class="dropdown-item" href="Ayarlar.php">Ayarlar</a>
            <a class="dropdown-item" href="Cikis.php">Çıkış Yap</a>
          </div>
        </div>
      </span>
    </div>
  </nav>

  <!-- Page Container -->
  <div class="w3-content w3-margin-top" style="max-width:1400px;">

    <!-- The Grid -->
    <div class="w3-row-padding">

      <!-- Right Column -->
      <div class="w3-twothird col-md-12">

        <div class="w3-container w3-card w3-white w3-margin-bottom">
          <h5 class="w3-text-grey w3-padding-16"><a href="AnaSayfa.php"><i class="fa fa-arrow-left"></i></a><i class="fa fa-eye fa-fw w3-margin-right"></i>Tanı Önizleme
            <hr>
          </h5>



          <div class="w3-container">
            <div class="table-responsive" style="overflow-y: scroll; height:623px;">

              <center><div id="TaniListele">
<input type="hidden" name="tani" id="tani" value="<?php $illness_id = ($_GET['tani']); echo $illness_id; ?>"></input>
                </div></center>


            </div>
          </div>


        </div>
        <br>
        <div class="w3-container">
          <br>
        </div>
      </div>
      <!-- End Right Column -->
    </div>

    <!-- End Grid -->
  </div>

  <!-- End Page Container -->
  </div>

  <footer class="w3-container w3-teal w3-center w3-margin-top bg-purple">
    <p>Uludağ Üniversitesi Bilgisayar Mühendisliği Bitirme Projesi</p>

    <p> <a href="" target="_blank">GitHub</a></p>
  </footer>






</body>

</html>

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>


<script type="text/javascript">
  $(document).ready(function() {

    var illness_id = $('#tani').val();

    $.ajax({
      url: "TaniIzleApi.php",
      type: "POST", // #ya da POST
      data: {
        illness_id: illness_id
      },

      beforeSend: function() {
        $("#TaniListele").html('<div style="font-size:14px;" title="Tanılar Yükleniyor" >Tanılar yükleniyor...! <i class="fa fa-refresh fa-spin" style="font-size:17px"></i></div>');
      },

      success: function(TaniListele) {

        $("#TaniListele").html(TaniListele);

       // console.log(TaniListele);

      },
      error: function(error) {
        //  $("#district").html(error);
        console.log(`Error ${error}`);
      }

  });
  });
</script>

<script type="text/javascript">
function tik(obj) {
id = obj.getAttribute("id");
idr = obj.getAttribute("name");
token = obj.getAttribute("class");
            $.ajax({
                   url: "TaniAdiGetirIzleApi.php",
                   type: "POST", // #ya da POST
                   data: {linked_to:idr, token:token},
                          beforeSend:function(){
                                   $("#loading").html('<div style="font-size:15px;" title="Tanı Yükleniyor" >Tanı yükleniyor...! <i class="fa fa-refresh fa-spin" style="font-size:14px"></i></div>');

                                  },
                          success: function (response) {

                      parsedAdd =  $(response).find('.item').attr('id');

                             if (parsedAdd == '100'){
                        $("#100"+id).html(response);
                      } else if (parsedAdd == '99'){
                        $("#99"+id).html(response);
                      } else if (parsedAdd == '98'){
                        $("#98"+id).html(response);
                      } else if (parsedAdd == '97'){
                        $("#97"+id).html(response);
                      } else if (parsedAdd == '96'){
                        $("#96"+id).html(response);
                      } else if (parsedAdd == '95'){
                        $("#95"+id).html(response);
                      } else if (parsedAdd == '94'){
                        $("#94"+id).html(response);
                      } else if (parsedAdd == '93'){
                        $("#93"+id).html(response);
                      } else if (parsedAdd == '92'){
                        $("#92"+id).html(response);
                      } else if (parsedAdd == '91'){
                        $("#91"+id).html(response);
                      } else if (parsedAdd == '90'){
                        $("#90"+id).html(response);
                      } else if (parsedAdd == '89'){
                        $("#89"+id).html(response);
                      } else if (parsedAdd == '88'){
                        $("#88"+id).html(response);
                      } else if (parsedAdd == '87'){
                        $("#87"+id).html(response);
                      } else if (parsedAdd == '86'){
                        $("#86"+id).html(response);
                      } else if (parsedAdd == '85'){
                        $("#85"+id).html(response);
                      } else if (parsedAdd == '84'){
                        $("#84"+id).html(response);
                      } else if (parsedAdd == '83'){
                        $("#83"+id).html(response);
                      } else if (parsedAdd == '82'){
                        $("#82"+id).html(response);
                      } else if (parsedAdd == '81'){
                        $("#81"+id).html(response);
                      } else if (parsedAdd == '80'){
                        $("#80"+id).html(response);
                      } else if (parsedAdd == '79'){
                        $("#79"+id).html(response);
                      } else if (parsedAdd == '78'){
                        $("#78"+id).html(response);
                      } else if (parsedAdd == '77'){
                        $("#77"+id).html(response);
                      } else if (parsedAdd == '76'){
                        $("#76"+id).html(response);
                      } else if (parsedAdd == '75'){
                        $("#75"+id).html(response);
                      } else if (parsedAdd == '74'){
                        $("#74"+id).html(response);
                      } else if (parsedAdd == '73'){
                        $("#73"+id).html(response);
                      } else if (parsedAdd == '72'){
                        $("#72"+id).html(response);
                      } else if (parsedAdd == '71'){
                        $("#71"+id).html(response);
                      } else if (parsedAdd == '70'){
                        $("#70"+id).html(response);
                      } else if (parsedAdd == '69'){
                        $("#69"+id).html(response);
                      } else if (parsedAdd == '68'){
                        $("#68"+id).html(response);
                      } else if (parsedAdd == '67'){
                        $("#67"+id).html(response);
                      } else if (parsedAdd == '66'){
                        $("#66"+id).html(response);
                      } else if (parsedAdd == '65'){
                        $("#65"+id).html(response);
                      } else if (parsedAdd == '64'){
                        $("#64"+id).html(response);
                      } else if (parsedAdd == '63'){
                        $("#63"+id).html(response);
                      } else if (parsedAdd == '62'){
                        $("#62"+id).html(response);
                      } else if (parsedAdd == '61'){
                        $("#61"+id).html(response);
                      } else if (parsedAdd == '60'){
                        $("#60"+id).html(response);
                      } else if (parsedAdd == '59'){
                        $("#59"+id).html(response);
                      } else if (parsedAdd == '58'){
                        $("#58"+id).html(response);
                      } else if (parsedAdd == '57'){
                        $("#57"+id).html(response);
                      } else if (parsedAdd == '56'){
                        $("#56"+id).html(response);
                      } else if (parsedAdd == '55'){
                        $("#55"+id).html(response);
                      } else if (parsedAdd == '54'){
                        $("#54"+id).html(response);
                      } else if (parsedAdd == '53'){
                        $("#53"+id).html(response);
                      } else if (parsedAdd == '52'){
                        $("#52"+id).html(response);
                      } else if (parsedAdd == '51'){
                        $("#51"+id).html(response);
                      } else if (parsedAdd == '50'){
                        $("#50"+id).html(response);
                      } else if (parsedAdd == '49'){
                        $("#49"+id).html(response);
                      } else if (parsedAdd == '48'){
                        $("#48"+id).html(response);
                      } else if (parsedAdd == '47'){
                        $("#47"+id).html(response);
                      } else if (parsedAdd == '46'){
                        $("#46"+id).html(response);
                      } else if (parsedAdd == '45'){
                        $("#45"+id).html(response);
                      } else if (parsedAdd == '44'){
                        $("#44"+id).html(response);
                      } else if (parsedAdd == '43'){
                        $("#43"+id).html(response);
                      } else if (parsedAdd == '42'){
                        $("#42"+id).html(response);
                      } else if (parsedAdd == '41'){
                        $("#41"+id).html(response);
                      } 
                      $("#loading").html('');

                      console.log(response,id);

                 },
                 error: function (error) {
                 //  $("#district").html(error);
                   console.log(`Error ${error}`);
                 }
               });
  };
</script>
<?php
}else{
echo '<script type="text/javascript">
     window.location = "GirisYap.php"
</script>';

}
?>