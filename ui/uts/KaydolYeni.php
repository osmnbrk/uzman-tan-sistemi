
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="userislemleri.css">

    <title>Kaydol - UZman Tanı Sistemi</title>
  </head>
  <body>
    <div class="vid-container">

  <div class="inner-container">

    <div class="box">
      <h1 class="text-dark">Kaydol</h1>
      <input type="text" name="name" id="name" placeholder="Ad Soyad"/>
      <input type="email"  name="email" id="email" placeholder="E-mail"/>
      <input type="password" name="password" id="password"  placeholder="Şifre"/>
      <button id="giris" onclick="Kaydol(this)">Kaydol</button>
        <p><a href="GirisYap.php">Zaten Üyeyim</a></p>
      <div id="SonucYaz">

      </div>
    </div>
  </div>
</div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

  </body>
</html>

<script type="text/javascript">
  function Kaydol() {


    var name = $("#name").val();
    var email = $("#email").val();
    var password = $("#password").val();



    if (name == '' || email == '' || password == "") {
      $('#uyari').html('<span class="text-danger">Boş Alanları Doldurunuz...!</span>');

    } else {


      $.ajax({
        url: "KaydolApi.php",
        type: 'POST', // #ya da POST
        //  data : $("form").serializeArray(),
        data: {
          name: name,
          email: email,
          password: password,
        },
        beforeSend: function() {
          $("#kaydet").html('<div style="font-size:11px;" title="kayıt yapılıyor " >kayıt yapılıyor  <i class="fa fa-refresh fa-spin" style="font-size:14px"></i></div>');
        },

        success: function(response) {

          $("#kaydet").html('Kaydol');
          $("#kaydet").attr("disabled", false);
          $("#SonucYaz").html(response);


          console.log(response);

        },
        error: function(error) {
          //  $("#district").html(error);
          console.log(`Error ${error}`);
        }
      });
    }
  };
</script>
