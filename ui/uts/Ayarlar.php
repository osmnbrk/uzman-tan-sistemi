<?php
session_start();
if($_SESSION["email"]!=''){
?>
<!doctype html>
<html lang="tr">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="uts.css">

  <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
  <link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Roboto'>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<!--   <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"> -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.5/css/bulma.min.css" />
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">

  <title>Ayarlar - Uzman Tanı Sistemi</title>
</head>


<body class="bg-light">
  <nav class="navbar navbar-expand-lg navbar-light bg-purple  sticky-top ">
    <a class="navbar-brand " href="AnaSayfa.php">Uzman Tanı Sistemi</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarText">
      <ul class="navbar-nav mr-auto">

      </ul>
      <span class="navbar-text">
        <div class="btn-group">
          <button type="button" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <svg class="bi bi-person-fill" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
              <path fill-rule="evenodd" d="M3 14s-1 0-1-1 1-4 6-4 6 3 6 4-1 1-1 1H3zm5-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6z" />
            </svg>
          </button>
          <div class="dropdown-menu dropdown-menu-right">
            <a class="dropdown-item" href="Ayarlar.php">Ayarlar</a>
            <a class="dropdown-item" href="Cikis.php">Çıkış Yap</a>
          </div>
        </div>
      </span>
    </div>
  </nav>

  <div class="container p-2">
    <div class="mx-auto bg-white col-md-5 rounded p-2">
      <p> </p>
      <h5 class="col">Kullanıcı Bilgileri</h5>
      
        <fieldset disabled>
          <div class="form-group">
            <label for="disabledTextInput" class="col col-form-label">Adı Soyadı</label>
            <div class="col">
              <input type="text" id="name" class="form-control" placeholder="Disabled input" value="<?php echo $_SESSION["name"];?>">
            </div>
          </div>
          <div class="form-group">
            <label for="disabledTextInput" class="col-sm-2 col-form-label">Mail</label>
            <div class="col">
              <input type="text" id="email" class="form-control" placeholder="Disabled input" value="<?php echo $_SESSION["email"];?>">
            </div>
          </div>
        </fieldset>
      

      <hr>

      <h5 class="col">Şifremi Değiştir</h5>
      
        <div class="form-group">
          <label for="inputPassword3" class="col col-form-label">Eski Şifre</label>
          <div class="col">
            <input type="password" class="form-control" id="mpass" placeholder="Password" value="">
          </div>
        </div>

        <div class="form-group">
          <label for="inputPassword3" class="col col-form-label">Yeni Şifre</label>
          <div class="col">
            <input type="password" class="form-control" id="ypass" placeholder="Password" value="">
          </div>
        </div>

        <div class="form-group">
          <label for="inputPassword3" class="col col-form-label">Yeni Şifre (Tekrar)</label>
          <div class="col">
            <input type="password" class="form-control" id="ytpass" placeholder="Password" value="">
          </div>
        </div>

        <div class="form-group">
          <div class="col">
            <button class="btn bg-purple text-white" id="degis" onclick="SifreDegis()">Şifre Değiştir</button>
            <div  id="uyari"></div>
          </div>
        </div>

    </div>
  </div>


  <footer class="w3-container w3-teal w3-center w3-margin-top bg-purple">
    <p>Uludağ Üniversitesi Bilgisayar Mühendisliği Bitirme Projesi</p>

    <p> <a href="" target="_blank">GitHub</a></p>
  </footer>

  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>


</body>
<script type="text/javascript">

function SifreDegis(){

      var email = $('#email').val();
      var mpass = $('#mpass').val();
      var ypass = $('#ypass').val();
      var ytpass = $('#ytpass').val();

      if (email == '' || mpass == '' || ypass == '' || ytpass == '') {

      //  alert('Lütfen şifre giriniz.');
        $('#uyari').html('<span class="text-danger">Boş Alanları Doldurunuz...!</span>');
        //return false;
      } else {

        $.ajax({
          url: "AyarlarSifreDegisApi.php",
          type: "POST", // #ya da POST
          data: {email:email, mpass:mpass, ypass:ypass, ytpass:ytpass},

              beforeSend: function() {

                    $("#degis").html('<div style="font-size:16px;" title="Lütfen Bekleyiniz" >Lütfen Bekleyiniz <i class="fa fa-refresh fa-spin" style="font-size:16px"></i></div>');
                    $("#degis").attr("disabled", true);
                  },

          success: function(response) {

             $("#uyari").html('');
             $("#degis").html('Şifre Değiştir');
             $("#degis").attr("disabled", false);
           // alert(response);
           // alert(response);
            $("#uyari").html(response);

          },
          error: function(error) {
            //  $("#district").html(error);
            console.log(`Error ${error}`);
          }
        });


      }

      };
</script>





<?php
}else{
echo '<script type="text/javascript">
     window.location = "GirisYap.php"
</script>';

}
?>