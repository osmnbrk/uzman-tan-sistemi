<?php
session_start();
if($_SESSION["email"]!=''){
?>
<?php

    $curl = curl_init();

    curl_setopt_array($curl, array(
      CURLOPT_URL => "http://ec2-52-59-243-11.eu-central-1.compute.amazonaws.com:9797/illness",
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "GET",
      CURLOPT_POSTFIELDS => "",
      CURLOPT_HTTPHEADER => array(
        "Content-Type: multipart/form-data; boundary=--------------------------947119203209062281309568"
      ),
    ));

    $response = curl_exec($curl);

    curl_close($curl);
  //  echo $response; //ekrana yazdır demek. Dizi olmayan değerleri.
      echo '    <div class="row d-flex justify-content-between">
            <input class="form-control col-11" id="myInput" type="text" placeholder="Search..">
            <div class="dropdown col-1">
              <button class="btn bg-purple dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <svg class="bi bi-plus text-light" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                  <path fill-rule="evenodd" d="M8 3.5a.5.5 0 0 1 .5.5v4a.5.5 0 0 1-.5.5H4a.5.5 0 0 1 0-1h3.5V4a.5.5 0 0 1 .5-.5z" />
                  <path fill-rule="evenodd" d="M7.5 8a.5.5 0 0 1 .5-.5h4a.5.5 0 0 1 0 1H8.5V12a.5.5 0 0 1-1 0V8z" />
                </svg>
              </button>
              <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                <a class="dropdown-item" href="HastalikKaydet.php">Hastalık ekle</a>
                <a class="dropdown-item" href="TaniKaydet.php">Hastalığa adım ekle</a>
              </div>

            </div>
          </div>
          <br>


      <table class="table table-bordered  bg-white">
        <thead>
          <tr>
            <th>Hastalık</th>
            <th>Kategoriler</th>
          </tr>
        </thead>
        <tbody id="myTable">';

        $a=json_decode($response);
        foreach ( $a as $list) {
          $id=str_replace('#', '', $list->id);
        echo '<tr>
           <td><a href="TaniIzle.php?tani='.$id.'">'.$list->Name.'</a></td>
            <td>'.$list->Category.'</td>
          </tr>';
        }


        echo '</tbody>
      </table>';

?>

<script>
  $(document).ready(function() {
    $("#myInput").on("keyup", function() {
      var value = $(this).val().toLowerCase();
      $("#myTable tr").filter(function() {
        $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
      });
    });
  });
</script>
<?php
}else{
echo '<script type="text/javascript">
     window.location = "GirisYap.php"
</script>';

}
?>
