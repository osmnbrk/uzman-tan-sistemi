<?php
session_start();
if($_SESSION["email"]!='' && $_SESSION["title"]==1 ){
?>
<!DOCTYPE html>
<html>
<title>Tani İzle - Uzman Tanı Sistemi</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="uts.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel='stylesheet' href='https://fonts.googleapis.com/css?family=Roboto'>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.7.5/css/bulma.min.css" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">



<body class="w3-light-grey">
  <nav class="navbar navbar-expand-lg navbar-light bg-purple sticky-top">
    <a class="navbar-brand " href="AnaSayfa.php">Uzman Tanı Sistemi</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarText">
      <ul class="navbar-nav mr-auto">

      </ul>
      <span class="navbar-text">
        <div class="btn-group">
          <button type="button" class="btn dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            <svg class="bi bi-person-fill" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
              <path fill-rule="evenodd" d="M3 14s-1 0-1-1 1-4 6-4 6 3 6 4-1 1-1 1H3zm5-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6z" />
            </svg>
          </button>
          <div class="dropdown-menu dropdown-menu-right">
            <a class="dropdown-item" href="Ayarlar.php">Ayarlar</a>
            <a class="dropdown-item" href="Cikis.php">Çıkış Yap</a>
          </div>
        </div>
      </span>
    </div>
  </nav>

  <!-- Page Container -->
  <div class="w3-content w3-margin-top" style="max-width:1400px;">

    <!-- The Grid -->
    <div class="w3-row-padding">

      <!-- Left Column -->
      <div class="w3-third col-md-3">

        <div class="w3-white w3-text-grey w3-card-4">
          <div class="w3-display-container">
            <br><br>
            <br>
            <div class="w3-display-bottomleft w3-container w3-text-black">
              <h5 class="text-secondary">Hastalık Adımlarını Ekleme
                <hr>
              </h5>
            </div>
          </div>


          <div class="w3-container">
            <div class="form-group">
              <form action="">

                <p class="w3-large w3-text-theme"><b></i>Hastalık:</b></p>
                <div id="HastalikListe">
                  <select class="custom-select" onchange="HastalikSec(selectObject)" name="illness_id" id="illness_id">
                    <option value="">Lütfen hastalık seçiniz</option>
                  </select>
                </div>
                <br>
                <p class="w3-large w3-text-theme"><b>Üst Tanı:</b></p>
                
                <strong id="linked_to_txt_lo"></strong>
                <textarea rows='5' class="form-control" id="linked_to_txt" disabled="true"></textarea>
                
                <input type="hidden" name="linked_to" id="linked_to" value="" required /><br>

                <p class="w3-large w3-text-theme"><b></i>Tanı:</b></p>
                <textarea class="form-control" type="text" name="name" id="name" value="" required></textarea><br>


                <p class="w3-large w3-text-theme"><b>Renk:</b></p>
                <div id="RenkListe">
                  <select class="custom-select" name="type" id="type" value="" required>
                    <option value="">Lütfen renk seçiniz</option>
                    <option value="#ffffb3">Sarı</option>
                    <option value="#6dbd86">Yeşil</option>
                  </select>
                </div>

                </br>
                <p class="w3-large w3-text-theme"><b></i>Bilgi:</b></p>
                <textarea class="form-control" type="text" name="explanation" id="explanation" value=""></textarea><br>


              </form>
            </div>

            <div class="btn bg-purple text-light" type="button" id="kaydet" class="button is-small is-link">Kaydet</div>


            <br>
            <div class="clearfix"></div>
            <div id="SonucYaz"></div>



            <br>
          </div>
        </div>

        <!-- End Left Column -->
      </div>

      <!-- Right Column -->
      <div class="w3-twothird col-md-9">

        <div class="w3-container w3-card w3-white w3-margin-bottom">
          <h5 class="w3-text-grey w3-padding-16"><i class="fa fa-eye fa-fw w3-margin-right"></i>Tanı Önizleme
            <hr>
          </h5>
          <div class="w3-container">
            <div class="table-responsive" style="overflow-y: scroll; height:623px;">
              <div id="TaniListele"></div>


            </div>
          </div>


        </div>
        <br>
        <div class="w3-container">
          <br>
        </div>
      </div>
      <!-- End Right Column -->
    </div>

    <!-- End Grid -->
  </div>

  <!-- End Page Container -->
  </div>

  <footer class="w3-container w3-teal w3-center w3-margin-top bg-purple">
    <p>Find me on social media.</p>
    <i class="fa fa-facebook-official w3-hover-opacity"></i>
    <i class="fa fa-instagram w3-hover-opacity"></i>
    <i class="fa fa-snapchat w3-hover-opacity"></i>
    <i class="fa fa-pinterest-p w3-hover-opacity"></i>
    <i class="fa fa-twitter w3-hover-opacity"></i>
    <i class="fa fa-linkedin w3-hover-opacity"></i>
    <p>Powered by <a href="https://www.w3schools.com/w3css/default.asp" target="_blank">w3.css</a></p>
  </footer>

</body>

</html>

<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>


<script type="text/javascript">
  function HastalikSec(selectObject) {

    var illness_id = selectObject.value;

    if (illness_id == '') {
      $('#SonucYaz').html('<span class="text-danger">Boş Alanları Doldurunuz...!</span>');
    }

    //var illness_id= $('#illness_id').val();
    // hastalık seçimi yapılınca seçlen hastalığın alt kümelerini çağırır
    $.ajax({
      url: "TaniListeleApi.php",
      type: "POST", // #ya da POST
      data: {
        illness_id: illness_id
      },

      beforeSend: function() {
        $("#TaniListele").html('<div style="font-size:14px;" title="Tanılar Yükleniyor" >Tanılar yükleniyor...! <i class="fa fa-refresh fa-spin" style="font-size:17px"></i></div>');
      },

      success: function(TaniListele) {

        $("#TaniListele").html(TaniListele);

        console.log(TaniListele);

      },
      error: function(error) {
        //  $("#district").html(error);
        console.log(`Error ${error}`);
      }



    });

  };
</script>



<script type="text/javascript">
  $(document).ready(function() {

    // sayfa yüklenirken hastalıkları açılır pencereye yükler
    $.ajax({
      url: "OptionHastalikApi.php",
      type: "GET", // #ya da POST

      beforeSend: function() {
        $("#HastalikListe").html('<div style="font-size:11px;" title="Hastalıklar Yükleniyor" >Hastalıklar yükleniyor...! <i class="fa fa-refresh fa-spin" style="font-size:14px"></i></div>');
      },

      success: function(hastaliklar) {

        $("#HastalikListe").html(hastaliklar);

        console.log(hastaliklar);

      },
      error: function(error) {
        //  $("#district").html(error);
        console.log(`Error ${error}`);
      }
    });

  });
</script>

<script type="text/javascript">
  $('#kaydet').click(function() {

    var illness_id = $('#illness_id').val();
    var name = $("#name").val();
    var type = $("#type").val();
    var linked_to = $("#linked_to").val();

    if (illness_id == "1") {
      $('#SonucYaz').html('<span class="text-danger">Lütfen Hastalık Seçiniz...!</span>');
    } else if (linked_to == "") {
      $('#SonucYaz').html('<span class="text-danger">Lütfen Sağ Taraftaki Diagramdan Üst Tanı Seçiniz...!</span>');
    } else if (name == "") {
      $('#SonucYaz').html('<span class="text-danger">Lütfen Tanı Adı Yazınız...!</span>');
    } else if (type == "") {
      $('#SonucYaz').html('<span class="text-danger">Lütfen Renk Seçiniz...!</span>');
    } else {

      $.ajax({
        url: "TaniKaydetApi.php",
        type: "POST", // #ya da POST
        data: $("form").serializeArray(),

        beforeSend: function() {
          $("#kaydet").html('<div style="font-size:11px;" title="Lütfen Bekleyiniz" >Lütfen Bekleyiniz...! <i class="fa fa-refresh fa-spin" style="font-size:14px"></i></div>');
          $("#kaydet").attr("disabled", true);
        },
        success: function(response) {
          $("#SonucYaz").html(response);
          $("#kaydet").html('Kaydet');
          $("#kaydet").attr("disabled", false);

          // kayıt işlemi başarılıysa tanı önizleme ekranını yeniliyoruz.
          $.ajax({
            url: "TaniListeleApi.php",
            type: "POST", // #ya da POST
            data: {
              illness_id: illness_id
            },
            beforeSend: function() {
              $("#TaniListele").html('<div style="font-size:14px;" title="Tanılar Yükleniyor" >Tanılar yükleniyor...! <i class="fa fa-refresh fa-spin" style="font-size:17px"></i></div>');
            },
            success: function(TaniListele) {
              $("#TaniListele").html(TaniListele);
              // console.log(TaniListele);
            },
            error: function(error) {
              //  $("#district").html(error);
              console.log(`Error ${error}`);
            }
          });

          $("#linked_to").val('');
          $('#linked_to_txt').text('');
          $("#name").val('');
          $("#type").val('');
        },
        error: function(error) {
          //  $("#district").html(error);
          console.log(`Error ${error}`);
        }
      });
    }
  });
</script>
<?php
}else{
echo '<script type="text/javascript">
      alert("Yetkniz Yok");
     window.location = "AnaSayfa.php"
</script>';

}
?>
